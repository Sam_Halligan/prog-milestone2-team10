﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team_Menu
{
    class Program
    {
        static void Main(string[] args)
        {

            var menu = "x";



            do
            {

                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"******************************");
                Console.WriteLine($"*          TEAM 10 MENU      *");
                Console.WriteLine($"*                            *");
                Console.WriteLine($"*    1. Task One             *");
                Console.WriteLine($"*    2. Task Two             *");
                Console.WriteLine($"*    3. Task Three           *");
                Console.WriteLine($"*    4. Task Four            *");
                Console.WriteLine($"*                            *");
                Console.WriteLine($"******************************");
                Console.WriteLine();
                Console.WriteLine($"Please select a task below!");

                menu = Console.ReadLine();



                if (menu == "1")
                {
                    Console.Clear();
                    datecalc();
                }
                else if (menu == "2")
                {
                    Console.Clear();
                    Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkDict = new Dictionary<Dictionary<string, string>, Dictionary<string, int>>();
                    List<string> paperCodes = new List<string>();
                    List<int> MarkList = new List<int>();
                    List<string> letterMarks = new List<string>();
                    MarkDict = setMarks();
                    paperCodes = getPaperCodes(MarkDict);
                    MarkList = getMarkInfo(MarkDict);
                    letterMarks = getLetterMarks(MarkList);
                    printMarks(MarkDict, paperCodes, MarkList, letterMarks);
                    calculateAverage(MarkList);
                    list1(MarkList, paperCodes);
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("Do you want to use the program again? 1 for yes /  2 to return to menu");
                    int answer = Convert.ToInt32(Console.ReadLine());
                    if (answer == 1)
                    {
                        {
                            Console.Clear();
                        }
                        Main(args);
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Type M to return to the main menu");
                        menu = Console.ReadLine();

                    }




                }
                else if (menu == "3")
                {
                    Console.Clear();
                    Generator();
                    Console.Clear();
                    Console.WriteLine("Type M to return to the main menu");
                    menu = Console.ReadLine();


                }
                else if (menu == "4")
                {
                    Console.Clear();

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Type M to return to the main menu");
                    menu = Console.ReadLine();

                }
            } while (menu == "M");

        }
        static void datecalc()

        {

            Console.WriteLine("Please enter your date of birth! (dd/mm/yyyy)");
            var dob = Console.ReadLine();

            string birthDateString = $"{dob}";
            DateTime birthDate;
            if (DateTime.TryParse(birthDateString, out birthDate))
            {
                DateTime today = DateTime.Now;
                Console.WriteLine("You are {0} days old", (today - birthDate).Days);
            }
            else Console.WriteLine("Incorrect date format!");
        }
        public static List<string> getLetterMarks(List<int> x)
        {
            List<string> Marks = new List<string>();

            foreach (int i in x)
            {
                string Mark;
                if (i >= 90)
                {
                    Mark = "A+";
                }
                else if (i <= 89 && i >= 85)
                {
                    Mark = "A";
                }
                else if (i <= 84 && i >= 80)
                {
                    Mark = "A-";
                }
                else if (i <= 79 && i >= 75)
                {
                    Mark = "B+";
                }
                else if (i <= 74 && i >= 70)
                {
                    Mark = "B";
                }
                else if (i <= 69 && i >= 65)
                {
                    Mark = "B-";
                }
                else if (i <= 64 && i >= 60)
                {
                    Mark = "C+";
                }
                else if (i <= 59 && i >= 55)
                {
                    Mark = "C";
                }
                else if (i <= 54 && i >= 50)
                {
                    Mark = "C-";
                }
                else if (i <= 49 && i >= 40)
                {
                    Mark = "D";
                }
                else
                {
                    Mark = "E";
                }

                Marks.Add(Mark);
            }

            return Marks;
        }
        public static List<string> getPaperCodes(Dictionary<Dictionary<string, string>, Dictionary<string, int>> x)
        {
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            List<string> paperCodes = new List<string>();
            Dictionary<string, string> levelID = x.Keys.ElementAt(0);
            x.TryGetValue(levelID, out paperInfo);

            foreach (KeyValuePair<string, int> entry in paperInfo)
            {
                paperCodes.Add(entry.Key);
            }

            return paperCodes;
        }

        public static List<int> getMarkInfo(Dictionary<Dictionary<string, string>, Dictionary<string, int>> x)
        {
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            List<int> MarkInfo = new List<int>();
            Dictionary<string, string> levelID = x.Keys.ElementAt(0);
            x.TryGetValue(levelID, out paperInfo);

            foreach (KeyValuePair<string, int> entry in paperInfo)
            {
                MarkInfo.Add(entry.Value);
            }

            return MarkInfo;
        }


        public static void calculateAverage(List<int> x)
        {
            double average = 0;
            int sum = 0;

            foreach (int i in x)
            {
                sum += i;
            }

            average = sum / x.Count();
            Console.WriteLine($"The average of the entered marks is: {average}");

            if (average <= 100 && average >= 50)
            {
                Console.WriteLine($"Overall the student has passed the year. ");
            }
            else
            {
                Console.WriteLine($"Overall the student has failed the year. ");
            }
        }

        public static void printMarks(Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkDict, List<string> paperCodes, List<int> MarkInfo, List<string> letterInfo)
        {
            Dictionary<string, string> levelID = new Dictionary<string, string>();
            string studentID;
            string level;
            levelID = MarkDict.Keys.ElementAt(0);
            studentID = levelID.Keys.ElementAt(0);
            level = levelID.Values.ElementAt(0);

            Console.WriteLine($"Student ID: {studentID}");
            Console.WriteLine($"Level of study: DAC{level}");
            Console.WriteLine($"Papers entered: {String.Join(",", paperCodes)}");
            Console.WriteLine($"Paper Percentages: {String.Join(",", MarkInfo)}");
            Console.WriteLine($"Paper Letter Grades: {String.Join(",", letterInfo)}");
        }


        public static void list1(List<int> MarkInfo, List<string> paperCodes)
        {
            List<string> highPapers = new List<string>();

            for (int i = 0; i < MarkInfo.Count(); i++)
            {
                if (MarkInfo.ElementAt(i) >= 90)
                {
                    highPapers.Add(paperCodes.ElementAt(i));
                }
            }

            if (highPapers.Count() > 0)
            {
                Console.WriteLine($"The student scored an 'A+' letter Grade in the following papers: {String.Join(", ", highPapers)}");
            }
            else
            {
                Console.WriteLine($"The student did not score an 'A+' in any subject.");
            }
        }

        public static Dictionary<Dictionary<string, string>, Dictionary<string, int>> setMarks()
        {
            int paperAmount = 0;
            bool validCheck = true;
            string level = "";
            Dictionary<string, string> levelID = new Dictionary<string, string>();
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkInfo = new Dictionary<Dictionary<string, string>, Dictionary<string, int>>();

            while (validCheck)
            {
                Console.WriteLine($"Please enter what level you are in using an integer:");
                level = Console.ReadLine();
                Console.Clear();

                if (level == "6")
                {
                    paperAmount = 3;
                    validCheck = false;
                }
                else if (level == "5")
                {
                    paperAmount = 4;
                    validCheck = false;
                }
                else
                {
                    Console.WriteLine($"Error: This is not a valid level please try again.");
                }
            }

            for (int i = 0; i < paperAmount; i++)
            {
                Console.WriteLine($"Please enter the paper code for paper {i + 1}:");
                string paperCode = Console.ReadLine();
                paperInfo.Add(paperCode, 0);
                Console.Clear();
            }

            Console.WriteLine($"Please input your Student ID Number:");
            string studentID = Console.ReadLine();
            Console.Clear();

            for (int i = 0; i < paperAmount; i++)
            {
                validCheck = true;
                while (validCheck)
                {
                    Console.WriteLine($"Please enter one of the paper codes:");
                    string paperCode = Console.ReadLine();
                    int paperMark = 0;
                    Console.Clear();

                    if (paperInfo.TryGetValue(paperCode, out paperMark))
                    {
                        paperMark = 0;
                        bool MarkCheck = true;

                        while (MarkCheck)
                        {
                            Console.WriteLine($"Please enter your mark for this paper rounded to the nearest %: ");
                            try
                            {
                                paperMark = int.Parse(Console.ReadLine());
                                Console.Clear();
                                if (paperMark <= 100 && paperMark >= 0)
                                {
                                    paperInfo.Remove(paperCode);
                                    paperInfo.Add(paperCode, paperMark);
                                    MarkCheck = false;
                                }
                                else
                                {
                                    Console.WriteLine($"Error: Please enter an integer between 0 and 100.");
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine($"Error: Invalid Mark.");
                            }
                        }

                        validCheck = false;
                    }
                    else
                    {
                        Console.WriteLine($"Error: Invalid paper code. ");
                    }
                }
            }
            levelID.Add(studentID, level);
            MarkInfo.Add(levelID, paperInfo);
            return MarkInfo;
        }

        public static void Main2(string[] args)
        {

        }


        static void Generator()
        {
            int Score = 0;
            for (int i = 1; i <= 5; i++)
            {

                Console.WriteLine("Enter a whole number between 1-5");
                int x = Convert.ToInt32(Console.ReadLine());

                Random random = new Random();
                int randomNumber = random.Next(1, 6);

                Console.WriteLine(randomNumber);

                if (x == randomNumber)
                {
                    Console.WriteLine("you got it right");
                    Score++;
                }

                else
                {
                    Console.WriteLine("Sorry you guessed wrong");
                }
            }
            Console.Write("Your score is: ");
            Console.WriteLine(Score);
            Console.WriteLine("Do you want to play again? 1 for yes /  2 for no");
            int answer = Convert.ToInt32(Console.ReadLine());

            if (answer == 1)
            {
                NewGame(Score);
            }

            else
            {
                Console.WriteLine("Thanks for playing");
            }
        }

        static void NewGame(int Score)
        {
            Console.Write("Last time you got: ");
            Console.WriteLine(Score);

            Score = 0;
            for (int i = 1; i <= 5; i++)
            {

                Console.WriteLine("Enter a whole number between 1-5");
                int x = Convert.ToInt32(Console.ReadLine());

                Random random = new Random();
                int randomNumber = random.Next(1, 6);

                Console.WriteLine(randomNumber);

                if (x == randomNumber)
                {
                    Console.WriteLine("you got it right");
                    Score++;
                }

                else
                {
                    Console.WriteLine("Sorry you guessed wrong");
                }
            }
            Console.WriteLine(Score);
            Console.WriteLine("Do you want to play again? 1 for yes /  2 for no");
            int answer = Convert.ToInt32(Console.ReadLine());

            if (answer == 1)
            {
                NewGame(Score);
            }

            else
            {
                Console.WriteLine("Thanks for playing");
            }
        }
    }
}
